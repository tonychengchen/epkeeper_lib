# 版本日志


### *Ver 1.0.20*
***
- fix bug

### *Ver 1.0.19*
***
- add microgrid.epk.PVWattsAPI

### *Ver 1.0.18*
***
- add common.exceptions

### *Ver 1.0.16-17*
***
- add nrel_api

### *Ver 1.0.13-15*
***
- fix MysqlPool.insert_df

### *Ver 1.0.10-12*
***
- fix bug

### *Ver 1.0.9*
***
- add TOU_HOUR code

### *Ver 1.0.8*
***
- 加入README和HISTORY

