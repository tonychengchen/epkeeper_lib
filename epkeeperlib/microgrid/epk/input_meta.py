INPUT_META = {
    'Scenario': {
        'description': 'price1',
        'Site': {
            'latitude': 31.19702,
            'longitude': 121.270629,
            'land_acres': 0,
            'roof_squarefeet': 1291668.0,
            'Financial': {
                'third_party_ownership': False,
                'escalation_pct': 0,
                'microgrid_upgrade_cost_pct': 0,
                'om_cost_escalation_pct': 0,
                'owner_discount_pct': 0,
                'owner_tax_pct': 0,
                'value_of_lost_load_us_dollars_per_kwh': 0,
                'analysis_years': 25,
                'offtaker_discount_pct': 0,
                'offtaker_tax_pct': 0.25
            },
            'LoadProfile': {
                'critical_load_pct': 0,
                'annual_kwh': 5000000,
                'doe_reference_name': 'FlatLoad_8_5'
            },
            'ElectricTariff': {
                'blended_annual_demand_charges_us_dollars_per_kw': 42,
                'blended_annual_rates_us_dollars_per_kwh': 0.6798299999999999,
                'wholesale_rate_above_site_load_us_dollars_per_kwh': 0.4146
            },
            'Storage': {
                'max_kw': 0
            },
            'PV': {
                'can_net_meter': False,
                'can_wholesale': False,
                'federal_itc_pct': 0,
                'gcr': 0.99,
                'location': 'roof',
                'macrs_bonus_pct': 0,
                'macrs_itc_reduction': 0,
                'macrs_option_years': 0,
                'can_export_beyond_site_load': True,
                'pbi_years': 0,
                'array_type': 1,
                'azimuth': 180,
                'degradation_pct': 0.008,
                'installed_cost_us_dollars_per_kw': 3138.57,
                'inv_eff': 0.96,
                'losses': 0.14,
                'module_type': 0,
                'om_cost_us_dollars_per_kw': 43.94,
                'tilt': 0.537,
                'max_kw': 1349.27,
                'min_kw': 1349.27
            }
        }
    }
}
