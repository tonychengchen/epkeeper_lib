

class MonitorError(Exception):
    """
    监控数据错误
    """
    pass


class InputError(Exception):
    """
    输入错误
    """
    pass


